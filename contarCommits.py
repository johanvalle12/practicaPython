import requests
import json
import matplotlib.pyplot as johan
request = requests.get("https://api.bitbucket.org/2.0/repositories/vargasmanuel/practicagit/commits")
Datos = json.loads(request.content)
cont = 0
contManuel = 0
contJohan = 0

for values in Datos['values']:
    Autor=values['author']
    raw=Autor['raw']
    if ((raw == "Manuel Vargas <vargas.manuel@uabc.edu.mx>") | (raw == "johanvalle12 <johan.valle@uabc.edu.mx>")):
         cont=cont+1
    if raw == "Manuel Vargas <vargas.manuel@uabc.edu.mx>":
        contManuel=contManuel+1
    else:
        contJohan=contJohan+1

print("Cantidad de commits: " + str(cont))
print("Cantidad de commits de Manuel: " + str(contManuel))
print("Cantidad de commits de Johan: " + str(contJohan))

johan.bar(["Johan", "Manuel"],[contJohan,contManuel])